'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const moment = require('moment');
const { MongoClient } = require('mongodb');

const port = 80;
const host = '0.0.0.0';
const mongoUrl = process.env.MONGO_URL;
const eventsCollectionName = process.env.EVENTS_COLLECTION_NAME;

const app = express();
app.use(bodyParser.json({limit: '50mb'}));

app.post('/collect', async (req, res) => {
    const events = (req?.body || []);

    const client = new MongoClient(mongoUrl);

    await client.connect(async (err) => {
        if(err){
            throw new Error(`Could not connect to DB: ${err}`);
        }

        const db = client.db();
        const collection = db.collection(eventsCollectionName);

        await Promise.all(events.map(event => {
            const { msg, duration } = event;
            const timestamp = moment(event.timestamp).toDate();
            const query = typeof event.query === 'string' ? event.query : null;

            return new Promise((resolve, reject) => {
                collection.insertOne({ timestamp, msg, duration, query }, ((err, result) => {
                    if(err){
                        reject(err);
                    } else {
                        resolve();
                    }
                }));
            });
        }));

        client.close();
        res.send({success: true});
    });
});

app.listen(port, host);
console.log(`running on http://${host}:${port}`);