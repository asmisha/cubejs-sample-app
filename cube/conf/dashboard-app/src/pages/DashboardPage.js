import React from 'react';
import { Col } from 'antd';
import ChartRenderer from '../components/ChartRenderer';
import Dashboard from '../components/Dashboard';
import DashboardItem from '../components/DashboardItem';
const DashboardItems = [
  {
    id: 0,
    name: 'Events count',
    vizState: {
      query: {
        measures: ['Events.count'],
        timeDimensions: [
          {
            dimension: 'Events.timestamp',
            granularity: 'minute',
          },
        ],
        order: {
          'Events.timestamp': 'asc',
        },
      },
      chartType: 'line',
    },
  },
  {
    id: 1,
    name: 'Database queries per minute',
    vizState: {
      query: {
        measures: ['Events.count'],
        timeDimensions: [
          {
            dimension: 'Events.timestamp',
            granularity: 'minute',
          },
        ],
        order: {
          'Events.timestamp': 'asc',
        },
        dimensions: [],
        filters: [
          {
            member: 'Events.msg',
            operator: 'equals',
            values: ['Query completed'],
          },
        ],
      },
      chartType: 'line',
    },
  },
  {
    id: 2,
    name: 'Top 10 SQL queries by total execution time',
    vizState: {
      query: {
        dimensions: ['Events.query'],
        timeDimensions: [
          {
            dimension: 'Events.timestamp',
          },
        ],
        order: {
          'Events.duration': 'desc',
        },
        measures: ['Events.duration'],
        filters: [
          {
            member: 'Events.duration',
            operator: 'set',
          },
          {
            member: 'Events.query',
            operator: 'set',
          },
        ],
        limit: 10,
      },
      chartType: 'table',
    },
  },
];

const DashboardPage = () => {
  const dashboardItem = (item) => (
    <Col
      span={24}
      lg={12}
      key={item.id}
      style={{
        marginBottom: '24px',
      }}
    >
      <DashboardItem title={item.name}>
        <ChartRenderer vizState={item.vizState} />
      </DashboardItem>
    </Col>
  );

  const Empty = () => (
    <div
      style={{
        textAlign: 'center',
        padding: 12,
      }}
    >
      <h2>
        There are no charts on this dashboard. Use Playground Build to add one.
      </h2>
    </div>
  );

  return DashboardItems.length ? (
    <Dashboard dashboardItems={DashboardItems}>
      {DashboardItems.map(dashboardItem)}
    </Dashboard>
  ) : (
    <Empty />
  );
};

export default DashboardPage;
