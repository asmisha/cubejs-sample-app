# Cube.js Sample App

## Configuration

- `cp .env.dist .env`
- `cp backend/.env.dist backend/.env`
- `cp .cubejs.env.dist .cubejs.env`
- Edit `.env`, `backend/.env` and `.cubejs.env` according to your needs.

## Run

`docker-compose up`

## Set Up Event Collection

By default the project is set up to collect it's own cube.js instance events.

To set up event collection from any other cube.js instance you need to set the `CUBEJS_AGENT_ENDPOINT_URL` environment variable for this instance:
`CUBEJS_AGENT_ENDPOINT_URL="http://host.docker.internal:3001/collect"`

It should point to the `backend` host and port.

## View the Dashboard

To view the dashboard open `http://localhost:3002/` in browser.

The URI should point to the `frontend` host and port.
