cube(`Events`, {
  sql: `SELECT * FROM events.events`,

  refreshKey: {
    every: `10 second`
  },

  joins: {
    
  },
  
  measures: {
    count: {
      type: `count`,
      drillMembers: [timestamp]
    },
    
    duration: {
      sql: `duration`,
      type: `max`
    }
  },
  
  dimensions: {
    msg: {
      sql: `msg`,
      type: `string`
    },
    
    query: {
      sql: `query`,
      type: `string`
    },
    
    timestamp: {
      sql: `timestamp`,
      type: `time`
    }
  },
  
  dataSource: `default`
});
